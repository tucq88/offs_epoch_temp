<?php include 'header.php' ?>

    <!-- Intro -->
    <div class="team-intro page-intro">
        <div class="section-title">
            <div class="container">
                <h1 class="title">Work with us</h1>
                <p>EPOCH Group is comprised of a highly talented and experienced team of people working in a highly collaborative environment. The team works hard but also enjoys a range of employee benefits to stay fresh, stay on top of their game and keep healthy and active.</p>
                <div class="divider"><img src="assets/img/divider.png" alt=""/></div>
            </div>
        </div>
    </div>

    <!-- Page Content -->
    <div class="page-work-us page-content">

        <section class="work-us-highlight container-fluid">
            <div class="row">
                <div class="img-wrapper img-bg col-sm-6" style="background-image: url('assets/img/origin/work.jpg');">
                </div>
                <div class="img-wrapper img-elem col-sm-6">
                    <img src="assets/img/origin/work.jpg" alt="" class="img-responsive"/>
                </div>
                <div class="desc-wrapper col-sm-6">
                    <h2 class="title">What it’s like to work at EPOCH</h2>
                    <p>Epoch is built on collaboration. By constantly learning from ourselves and from each other and keeping each other accountable, we achieve great results. Epoch’s employees are rewarded generously for embodying these foundations.</p>
                    <div class="blockquote">
                        <p>We are committed to creating an environment in which the right people thrive.</p>
                    </div>
                    <p>As an Epoch employee you will have access to countless benefits, a few examples are; additional leave allowances, insurance covers and company events that range from monthly pizza nights to weekends away.</p>
                    <p>When it comes to career aspirations at Epoch, the sky's the limit. You will go as far as your talent, commitment and drive will let you go and we are dedicated to help you achieve your aspirations.</p>
                </div>
            </div>
            <div class="row row-reversed">
                <div class="img-wrapper img-bg col-sm-6" style="background-image: url('assets/img/origin/candidate.jpg');">
                </div>
                <div class="img-wrapper img-elem col-sm-6">
                    <img src="assets/img/origin/candidate.jpg" alt="" class="img-responsive"/>
                </div>
                <div class="desc-wrapper col-sm-6">
                    <h2 class="title">The ideal candidate</h2>
                    <div class="blockquote">
                        <p>We are always looking for smart, highly driven people that are passionate about what they do.</p>
                    </div>
                    <p>Although expertise & technical skills are highly valued, we ask more from our people. Our competitive advantage is the result of a combination of factors, which includes a passion for what you do, hard work, curiosity, accountability, seeking out continuous improvement and a healthy challenge to the status quo. </p>
                    <p>We therefore believe that by giving people ownership of their work and challenging them on a daily basis we can help them reach their potential.</p>
                </div>
            </div>
            <div class="row">
                <div class="img-wrapper img-bg col-sm-6" style="background-image: url('assets/img/origin/howtoapply.jpg');">
                </div>
                <div class="img-wrapper col-sm-6">
                    <img src="assets/img/origin/howtoapply.jpg" alt="" class="img-responsive"/>
                </div>
                <div class="desc-wrapper col-sm-6">
                    <h2 class="title">How to apply</h2>
                    <p>If you have a passion for innovation, technology and the financial markets and you are interested in working with us please send us;</p>
                    <ul class="epoch-list">
                        <li>Cover letter</li>
                        <li>Resume</li>
                    </ul>
                    <hr/>
                    <div class="extend-text">
                        <a href="">careers@epochcapital.com.au</a>
                    </div>

                </div>
        </div>
        </section>

        <section class="work-us-jobs">
            <div class="container">

                <div class="section-title text-center">
                    <h3 class="title">Available Jobs</h3>
                    <p class="desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>

                <form action="" method="get">
                    <select name="job" onchange="this.form.submit()">
                        <option value="1">1 Job</option>
                        <option value="2">2 Jobs</option>
                        <option value="9">9 Jobs</option>
                    </select>
                </form>

                <div class="row">

                    <?php
                        $record = !empty($_GET['job']) ? $_GET['job'] : 1;
                        if ( $record == 1 ) {
                            $class = 'col-md-12 text-center';
                        } elseif ( $record == 2 ) {
                            $class = 'col-md-6';
                        } elseif ($record >= 3 ) {
                            $class = 'col-md-4';
                        }
                    ?>

                    <?php for ($i = 1; $i <= $record; ++$i) { ?>

                        <div class="<?= $class ?>">
                            <div class="job-item">
                                <div class="job-title-wrapper">
                                    <h4 class="title">Position:</h4>
                                    <p>Web Developer</p>
                                </div>

                                <div class="job-desc-wrapper">
                                    <h4 class="title">Job description:</h4>
                                    <div class="job-desc">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                            At blanditiis corporis doloribus laboriosam mollitia officiis quo recusandae?
                                            Ad, animi aperiam assumenda beatae distinctio eum magnam perspiciatis
                                            porro quaerat quam, repudiandae?</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                            At blanditiis corporis doloribus laboriosam mollitia officiis quo recusandae?
                                            Ad, animi aperiam assumenda beatae distinctio eum magnam perspiciatis
                                            porro quaerat quam, repudiandae?</p>
                                    </div>
                                </div>

                            </div>
                        </div>

                    <?php } ?>

                </div>

            </div>
        </section>

    </div>

<?php include 'footer.php' ?>