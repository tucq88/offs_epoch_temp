<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Epoch</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.css">
    <link rel="stylesheet" href="bower_components/fontawesome/css/font-awesome.css">
    <link rel="stylesheet" href="bower_components/bolster.bxSlider/jquery.bxslider.css">
    <link rel="stylesheet" href="assets/lib/pace/themes/blue/pace-theme-flash.css">
    <link rel="stylesheet" href="assets/lib/animate/animate.css">
    <link rel="stylesheet" href="assets/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!-- Navigation -->
<div class="menu-wrap">
    <a href="index.php">
        <img src="assets/img/logo-menu.png" alt="" class="logo-menu">
    </a>
    <nav class="menu">
        <div class="menu-list">
            <a href="index.php">Home</a>
            <a href="leadership-team.php">Leadership Team</a>
            <a href="work-with-us.php">Work with us</a>
            <a href="contact.php">Contact</a>
        </div>
    </nav>
    <div class="social-link">
        <a href="" class="linkedin"></a>
    </div>
</div>

<div class="content-wrap">
    <div class="top-bar clearfix">
        <i class="menu-button fa fa-bars" id="open-button"></i>
        <a href="index.php"><img src="assets/img/logo.png" class="pull-right"></a>
    </div>
