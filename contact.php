<?php include 'header.php' ?>

    <!-- Intro -->
    <div class="contact-intro page-intro">
        <div class="section-title">
            <div class="container">
                <h1 class="title">Contact Us</h1>
                <p>For all enquiries regarding the EPOCH Group please feel free to reach out to our team.</p>
                <div class="divider"><img src="assets/img/divider.png" alt=""/></div>
            </div>
        </div>
    </div>


    <!-- Page Content -->
    <div class="page-contact page-content">

        <section class="contact-detail">
            <div class="container">
                <div class="list-detail">
                    <div class="contact-item">
                        <div class="item-icon">
                            <img src="assets/img/contact/email.png" alt=""/>
                        </div>
                        <div class="item-desc">
                            <h4>Email us</h4>
                            <p>enquires@epochtradinggroup.com.au</p>
                        </div>
                    </div>
                    <div class="contact-item">
                        <div class="item-icon">
                            <img src="assets/img/contact/phone.png" alt=""/>
                        </div>
                        <div class="item-desc">
                            <h4>Give us a call</h4>
                            <p>+612 8070 3800</p>
                        </div>
                    </div>
                    <div class="col-sm-1"></div>

                </div>
            </div>
        </section>

        <section class="contact-map">
            <div class="google-map-contact-wrapper">
                <div class="get-in-touch">
                    <h3>Our Office</h3>
                    <div class="get-in-touch-wrapper">
                        <span class="icon">
                            <img src="assets/img/contact/map_marker.png" alt=""/>
                        </span>
                        <span class="address-wrapper">
                            <p>
                                Envato<br>
                                Level 13, 2 Elizabeth<br>
                                Victoria 3000 Australia
                            </p>
                        </span>
                    </div>
                </div>
            </div>
            <div id="google-map" style="height: 380px"></div>

        </section>

        <section class="contact-form">
            <div class="form-container container">
                <div class="row">
                    <div class="form-intro col-md-4">
                        <h3>Write a message</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. consectetur adipisicing elit.</p>
                        <div class="social-link">
                            <a class="linkedin" href=""></a>
                        </div>
                    </div>
                    <div class="form-inputs col-md-8">

                        <form action="">
                            <div class="row">
                                <div class="form-group col-sm-4">
                                    <label>Name:</label>
                                    <input type="text" class="form-control" required>
                                </div>
                                <div class="form-group col-sm-4">
                                    <label>Email address:</label>
                                    <input type="email" class="form-control" required>
                                </div>
                                <div class="form-group col-sm-4">
                                    <label>Subject:</label>
                                    <input type="text" class="form-control" required>
                                </div>
                                <div class="form-group col-md-12">
                                    <label>Message:</label>
                                    <textarea name="" class="form-control" cols="30" rows="10"></textarea>
                                </div>
                                <div class="form-group col-md-12">
                                    <button type="submit" class="btn btn-epoch">Send Message</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    </div>



<?php include 'footer.php' ?>