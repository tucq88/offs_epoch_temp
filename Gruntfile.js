'use strict';

module.exports = function (grunt) {
    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

    // Configurable paths
    var config = {
        lib: 'assets',
        view: 'View'
    };

    // Define the configuration for all the tasks
    grunt.initConfig({

        // Project settings
        config: config,

        watch: {
            sass: {
                files: ['<%= config.lib %>/scss/*.scss'],
                tasks: ['sass:server', 'autoprefixer']
            },
            livereload: {
                options: {livereload: 1906},
                files: [
                    '<%= config.lib %>/css/**/*',
                    '<%= config.lib %>/js/**/*',
                    '<%= config.lib %>/img/**/*',
                    '*.php'
                ]
            }
        },

        // Compiles Sass to CSS and generates necessary files if requested
        sass: {
            server: {
                options: {
                    sourceMap: true
                },
                files: [{
                    '<%= config.lib %>/css/style.css': '<%= config.lib %>/scss/style.scss'
                }]
            }
        },

        // Add vendor prefixed styles
        autoprefixer: {
            dist: {
                files: [{
                    '<%= config.lib %>/css/style.css': '<%= config.lib %>/css/style.css'
                }]
            }
        }

    });

    grunt.registerTask('serve', function (target) {
        grunt.task.run([
            'sass',
            'autoprefixer',
            'watch'
        ]);
    });
}
