    <footer>
        <div class="section-container container">
            <div class="row">

                <div class="site-info footer-col col-md-3">
                    <img src="assets/img/logo.png" alt="" class="logo">
                    <p class="footer-intro">We would love to hear from you, or check us out on our social media</p>
                    <div class="social-link">
                        <a href="" class="linkedin white"></a>
                    </div>
                </div>

                <div class="footer-menu footer-col col-md-3">
                    <h3 class="heading">Our Values</h3>
                    <ul class="epoch-list">
                        <li>Inspire</li>
                        <li>Optimise</li>
                        <li>Collaborate</li>
                        <li>Grow</li>
                    </ul>
                </div>

                <div class="contact-info footer-col col-md-6">
                    <div class="info-widget">
                        <h4 class="info-heading">
                            <i class="footicon envelope"></i> Our address:
                        </h4>
                        <p>
                            Level 12<br>
                            56 Pitt Street<br>
                            Sydney NSW 2000
                        </p>
                    </div>

                    <div class="info-widget">
                        <h4 class="info-heading">
                            <i class="footicon chat"></i> Email us
                        </h4>
                        <p>
                            <a href="mailto:enquiries@epochtradinggroup.com.au">enquiries@epochtradinggroup.com.au</a>
                        </p>
                    </div>

                    <div class="info-widget">
                        <h4 class="info-heading">
                            <i class="footicon phone"></i> Give us a call
                        </h4>
                        <p>
                             <a href="tel:+61280703800">+612 8070 3800</a>
                        </p>
                    </div>
                </div>

            </div>

            <hr class="full-width">

            <p class="copy">
                Information on this site and related material is the Copyright of Epoch Capital Pty Ltd unless otherwise noted.
            </p>
        </div>
    </footer>
    </div>


    <script src="bower_components/jquery/jquery.min.js"></script>
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="bower_components/fastclick/lib/fastclick.js"></script>
    <script src="bower_components/bolster.bxSlider/jquery.bxslider.min.js"></script>
    <script src="http://maps.googleapis.com/maps/api/js"></script>
    <!--<script src="assets/lib/pace/pace.min.js"></script>-->
    <script src="assets/lib/nicescroll/jquery.nicescroll.min.js"></script>
    <script src="assets/js/script.js"></script>
    <script src="http://localhost:1906/livereload.js"></script>
  </body>
</html>