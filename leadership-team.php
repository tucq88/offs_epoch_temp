<?php include 'header.php' ?>

<!-- Intro -->
<div class="team-intro page-intro">
    <div class="section-title">
        <div class="container">
            <h2 class="title">The Leadership team</h2>
            <p>The powerhouse of progress at EPOCH Group is the key leadership team. All with diverse and highly motivated backgrounds, the team works hard to deliver against ambitious KPI’s and strive towards the constant evolution of the wider EPOCH Group.</p>
            <div class="divider"><img src="assets/img/divider.png" alt=""/></div>
        </div>
    </div>
</div>

<!-- Page Content -->
<div class="page-team page-content">
    <div class="container">
        <div class="row">
            <?php for ($i = 1; $i <= 5; ++$i) { ?>
                <div class="col-md-6">
                    <div class="team-member row">
                        <div class="img-wrapper col-sm-6">
                            <img src="assets/img/team/member-<?= $i ?>.png" alt="">
                        </div>
                        <div class="desc-wrapper col-sm-6">
                            <div class="intro-mem">
                                <h3 class="title">Member <?= $i ?></h3>
                                <p class="subtitle">Just subtitle</p>
                                <hr>
                                <div class="desc">
                                    <p>Lorem ipsum dolor sit amet, ipsa mole officiis quas similique unde?</p>
                                </div>
                                <div class="social-link">
                                    <a class="linkedin" href=""></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>

<?php include 'footer.php' ?>