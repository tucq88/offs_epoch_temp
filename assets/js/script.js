$(function () {
    $('.home-intro').height($(window).height());
    //$('#home-video').height($(window).height());

    //Apply smooth scroll on desktop browser only
    if(!( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) )) {
        $('.content-wrap').niceScroll();
    }


    $('.content-wrap').scroll(function(e) {
        if ($(this).scrollTop() != 0) {
            $('.top-bar').addClass('no-bg-opacity');
        } else {
            $('.top-bar').removeClass('no-bg-opacity');
        }
    });

    $('#home-slider').bxSlider({
        "pagerType": 'short',
        'pagerSelector': $('.epoch-pager'),
        'nextSelector': $('.epoch-next'),
        'prevSelector': $('.epoch-prev')
    });

    $('.page-intro .title').addClass('animated fadeIn');

});

/**
 * Off-canvas Menu Show/Hide
 */
(function() {

    var bodyEl = document.body,
        content = document.querySelector( '.content-wrap' ),
        openbtn = document.getElementById( 'open-button' ),
        closebtn = document.getElementById( 'close-button' ),
        isOpen = false;

    function init() {
        initEvents();
    }

    function initEvents() {
        openbtn.addEventListener( 'click', toggleMenu );
        if( closebtn ) {
            closebtn.addEventListener( 'click', toggleMenu );
        }

        // close the menu element if the target it´s not the menu element or one of its descendants..
        content.addEventListener( 'click', function(ev) {
            var target = ev.target;
            if( isOpen && ( target !== openbtn ) ) {
                toggleMenu();
            }
        } );
    }

    function toggleMenu() {
        if( isOpen ) {
            //classie.remove( bodyEl, 'show-menu' );
            $(bodyEl).removeClass('show-menu');
        }
        else {
            //classie.add( bodyEl, 'show-menu' );
            $(bodyEl).addClass('show-menu');
        }
        isOpen = !isOpen;
    }

    init();

})();

/*
Active Google Map
 */
function google_map() {
    var latlng = new google.maps.LatLng(-33.8710, 151.2039);
    var myOptions = {
        zoom: 13,
        center: latlng,
        zoomControl: true,
        mapTypeControl: false,
        streetViewControl: false,
        scrollwheel: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        draggable: $(document).width() > 480,
        panControle: true
    };

    if (document.getElementById("google-map")) {
        var map = new google.maps.Map(document.getElementById("google-map"), myOptions);
        var marker = new google.maps.Marker({position: latlng, map: map});

        google.maps.event.addDomListener(window, "resize", function() {
            var center = map.getCenter();
            google.maps.event.trigger(map, "resize");
            map.setCenter(center);
        });
    }
}
jQuery(document).ready(function ($) {
    google_map();
});