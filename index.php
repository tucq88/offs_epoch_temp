<?php include 'header.php'; ?>

    <!-- Intro -->
    <div class="home-intro page-intro">
        <div class="intro-message">
            <!--<div class="container">-->
                <h1 class="animated bounceInDown">Where excellence comes to thrive</h1>
                <div class="container">
                    <p>Epoch is the meeting place of individual brilliance and team aspiration. To us winning is having our people thrive, being ahead of the game and continually leveraging on our strengths.</p>
                    <img src="assets/img/home/home-divider.png" alt=""/>
                </div>
            <!--</div>-->
        </div>
        <div class="bg-video">
            <video autoplay loop id="home-video">
                <source src="assets/home-video.mp4" type="video/mp4">
                <source src="assets/home-video.webm" type="video/webm">
            </video>
        </div>
    </div>

    <!-- About Us -->
    <section class="home-about">
        <div class="section-container container">
            <h2 class="title">What we are about</h2>
            <div class="row">
                <div class="block-text col-md-6">
                    <div class="block-title">Who is Epoch ?</div>
                    <p>Epoch is one of Australia’s leading international proprietary trading firms with its headquarters based in Sydney CBD. We have distinguished ourselves from our peers in the industry by maintaining world class technology in an innovative & collaborative team culture.</p>
                </div>
                <div class="block-text col-md-6">
                    <div class="block-title">Our Team</div>
                    <p>Epoch’s culture of teamwork fosters an environment where new ideas are constantly shared, developed & implemented. Our team comprises a wide variety of talented professionals including traders, technology specialists, financial engineers & algorithmic developers.</p>
                </div>
            </div>
        </div>
    </section>

    <!-- Quote - Parallax -->
    <div class="home-quote">
        <div class="quote-message">
            <div class="blockquote">
                <p>When nothing is sure, everything is possible</p>
                <span class="author">Margaret Drabble</span>
            </div>
        </div>
    </div>

    <!-- Skill -->
    <section class="home-skill">
        <div class="section-container container">
            <div class="section-title">
                <h2 class="title">What we stand for</h2>
                <p>Epoch has 4 pillars which underpin the strength of our company</p>
                <div class="divider">
                    <img src="assets/img/divider.png" alt=""/>
                </div>
            </div>

            <div class="row">
                <div class="block-icon col-sm-3">
                    <div class="icon-top">
                        <img src="assets/img/home/icon-inspire.png" alt="">
                    </div>
                    <h3 class="block-title">Inspire</h3>
                    <p>Create an environment inwhich people thrive</p>
                </div>
                <div class="block-icon col-sm-3">
                    <div class="icon-top">
                        <img src="assets/img/home/icon-optimise.png" alt="">
                    </div>
                    <h3 class="block-title">Optimise</h3>
                    <p>Capitalise on strengths tocapture the right opportunities</p>
                </div>
                <div class="block-icon col-sm-3">
                    <div class="icon-top">
                        <img src="assets/img/home/icon-collab.png" alt="">
                    </div>
                    <h3 class="block-title">Collaborate</h3>
                    <p>Embrace the knowledge of the collective to identify and maximise our approach</p>
                </div>
                <div class="block-icon col-sm-3">
                    <div class="icon-top">
                        <img src="assets/img/home/icon-grow.png" alt="">
                    </div>
                    <h3 class="block-title">Grow</h3>
                    <p>Seek and be prepared forquality and innovation</p>
                </div>
            </div>

        </div>
    </section>

    <!-- Slider -->
    <section class="home-slider">
        <ul class="bxslider" id="home-slider">
            <?php for ($i = 1; $i <= 5; ++$i) { ?>
                <li>
                    <div class="slide-item row">
                        <div class="img-wrapper col-sm-6">
                            <img src="assets/img/home/slider-1.png" alt="">
                        </div>
                        <div class="desc-wrapper col-sm-6">
                            <h2 class="title">PROFESSIONAL TRADING GROUP</h2>
                            <div class="desc">
                                <p>The Professional Trading Group (EPOCH PTG) is the professional’s trading arm of Epoch that specializes in catering for highly experienced traders with great expectations, who are seeking the highest standards in connectivity at the most competitive rates available in the market. </p>
                                <img src="assets/img/home/slide-divider.png" alt="" class="slide-divider"/>
                                Advance features:
                                <br><br>
                                <ul class="epoch-list">
                                    <li>Traders manage their own trade execution within the PTG</li>
                                    <li>Traders have access to benefits only larger trading firms receive</li>
                                    <li>Traders are able to maintain control over their own trading success</li>
                                </ul>
                                <hr>
                                <div class="extend-text">
                                    Are you a professional trader – email <a href="">link</a> for more information
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            <?php } ?>
        </ul>
        <div class="epoch-control">
            <a href="" class="epoch-next epoch-control-btn"><i class="fa fa-angle-up"></i></a>
            <div class="epoch-pager"></div>
            <a href="" class="epoch-prev epoch-control-btn"><i class="fa fa-angle-down"></i></a>
        </div>
    </section>

<?php include 'footer.php'; ?>
